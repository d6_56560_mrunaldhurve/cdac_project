package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.SocietyFlat;
import com.sunbeam.entities.Visitor;

public interface VisitorDao extends JpaRepository<Visitor, Integer>{
	
 Visitor findByName(String name);

Visitor findBySocietyflat(SocietyFlat findByFlatNoAndWing);
}
