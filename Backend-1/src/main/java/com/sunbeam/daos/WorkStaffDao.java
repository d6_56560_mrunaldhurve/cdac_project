package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.Owner;
import com.sunbeam.entities.WorkStaff;

public interface WorkStaffDao extends JpaRepository<WorkStaff, Integer> {
	
	List<WorkStaff> findAll();
	
	WorkStaff findById(int id);
	
	WorkStaff findByEmail(String email);
}
	
	
