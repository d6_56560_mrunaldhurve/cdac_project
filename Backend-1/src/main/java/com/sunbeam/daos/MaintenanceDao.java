package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.Maintenance;
import com.sunbeam.entities.SocietyFlat;

public interface MaintenanceDao extends JpaRepository<Maintenance, Integer> {
	
List<Maintenance> findAll();
 Maintenance findById(int id);
List<Maintenance> findByFlat(SocietyFlat flat);

}