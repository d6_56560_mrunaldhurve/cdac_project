package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.SocietyFlat;
import com.sunbeam.entities.Vehicle;

public interface VehicleDao extends JpaRepository<Vehicle, Integer>{

	Vehicle findById(int id);
	List<Vehicle> findAll();
	//Vehicle findByFlat(SocietyFlat societyFlat);
	Vehicle findByFlat(SocietyFlat findByFlatNoAndWing);
}
