package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.Tenant;

public interface TenantDao extends JpaRepository<Tenant, Integer> {

	List<Tenant> findAll();
	Tenant save(Tenant tenant);
	Tenant findById(int id);
}
