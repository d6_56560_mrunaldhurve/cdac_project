package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.Complaint;
import com.sunbeam.entities.Owner;
import com.sunbeam.entities.SocietyFlat;

public interface ComplaintDao extends JpaRepository<Complaint, Integer> {

	List<Complaint> findAll();
	Complaint findById(int id);
	List<Complaint> findByOwner(Owner owner);
}
