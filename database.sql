drop database project;
create database project;
use project;
drop table if exists  owner;
drop table if exists societyflat;

drop table if exists visitor;

drop table if exists complaint;
drop table if exists tenant;
drop table if exists maintenance;


drop table if exists noticeboard;

create table owner (id integer primary key auto_increment, 
firstName varchar(50),
lastName varchar(50),
gender varchar(50),
dob date,
mobileNo varchar(50),
email varchar(50),
role varchar(50),
password varchar(600));



create table societyflat(
id integer primary key auto_increment,
ownerId int ,
wing char(1), 
flatType varchar(5),
registerDate date, 
parkingSlot int , 
flatNo int , 
floorNo int,
FOREIGN KEY (ownerId) REFERENCES owner(id)
 );




create table visitor (
id integer primary key auto_increment, 
name varchar(50), 
flatId int, 
contactNo char(10) ,
inTime timestamp default CURRENT_TIMESTAMP, 
FOREIGN KEY (flatId) REFERENCES societyflat(id)
);


drop table if exists vehicle;
create table vehicle (
id integer primary key auto_increment,
flatId int 
,vehicleNo char(20) ,
FOREIGN KEY (flatId) REFERENCES societyflat(id)
);



create table complaint (
id integer primary key auto_increment, 
complaint varchar(600),
ownerId int , 
status varchar(20),
FOREIGN KEY (ownerId) REFERENCES owner(id)
);

create table tenant (
id integer primary key auto_increment, 
firstName varchar(20),
flatId int , 
lastName varchar(20),
contactNo varchar(10),
gender varchar(10),
email varchar(50)
);

drop table if exists maintenance;
create table maintenance (
id integer primary key auto_increment,
month varchar(10), 
flatId int ,
flatMaintenance double(10,2) 
,waterBill double(10,2) ,
parkingBill double(10,2),
others  double(10,2) , 
FOREIGN KEY (flatId) REFERENCES societyflat(id)
);

drop table if exists workstaff;
 create table workstaff (
id integer primary key auto_increment, 
name varchar(30),
 password varchar(600),
monthSalary double(10,2),
address varchar (100),
role varchar(20),
contactNo varchar(10),
email varchar(50)
);


 create table noticeboard (
id integer primary key auto_increment, 
noticeDate timestamp default CURRENT_TIMESTAMP ,
noticemsg varchar(100),
ownerId int,
FOREIGN KEY (ownerId) REFERENCES owner(id));






-- 

insert into owner  (id,firstName,lastName,gender,dob,mobileNo,email,role ,password) values (1,'Mrunal','Dhurve','Male','1998-05-10','7875254311','mrunaldhurve05@gmail.com','secretary','$2a$12$zIoRTG7tt.cXAYp4WLT2te/NfFofNNLDPjU8Qhwk3hhq8ZZ8WKQcu');
insert into owner(id,firstName,lastName,gender,dob,mobileNo,email,role ,password) values(2,'Amit', 'Kulkarni','Male', '1900-12-23','9876543210','amit@test.com', 'owner','$2a$10$H9PsSv9rLfYkvU6xZzACcenHp0heUe2J7jzOZTMd5Ov5zbX.YOicS');
insert into owner(id,firstName, lastName, email, role , password) values(3,'Nilesh', 'Ghule', 'nilesh@gmail.com', 'owner','$2a$10$LJeV/Fo8Gw7mTUCP7YiHpeiuKqcgRvUq.H19VDbsB2MSDupE7KXc2');


insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('A','2Bhk','2021-06-24',101,101,1);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('A','2Bhk','2021-01-24',102,102,1);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('A','2Bhk','2010-12-10',201,201,2);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('A','2Bhk','2015-06-24',202,202,2);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('A','2Bhk','2021-06-24',301,301,3);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('A','2Bhk','2021-01-24',302,302,3);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('B','3Bhk','2021-06-24',101,101,1);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('B','3Bhk','2021-01-24',102,102,1);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('B','3Bhk','2010-12-10',201,201,2);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('B','3Bhk','2015-06-24',202,202,2);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('B','3Bhk','2021-06-24',301,301,3);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('B','3Bhk','2021-01-24',302,302,3);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('C','2Bhk','2021-06-24',101,101,1);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('C','2Bhk','2021-01-24',102,102,1);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('C','2Bhk','2010-12-10',201,201,2);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('C','2Bhk','2015-06-24',202,202,2);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('C','2Bhk','2021-06-24',301,301,3);
insert into societyflat(wing,flatType,registerDate,parkingSlot,flatNo,floorNo) values ('C','2Bhk','2021-01-24',302,302,3);


insert into visitor(id,name,flatId,contactNo) values(1,'Rushikesh Patil',1,'9999999999');
insert into visitor(id,name,flatId,contactNo) values(2,'Thonos',4,'1111111111');



insert into vehicle (id,flatId,vehicleNo) values (1,3,'MH36 A 2568');
insert into vehicle (id,flatId,vehicleNo) values (2,2,'MH36 N 2280');


insert into complaint(id, complaint,ownerId,status) values (1,'there is leakage from flat 101',2,'pending');
insert into complaint(id, complaint,ownerId,status) values (2,'there is alot of noise from flat 101',3,'pending');


insert into tenant(id,firstName,flatId,lastName,contactNo,gender,email) values (1,"Sushant",1,"Pawar","1111122222","Male","sushantpawar@gmail.com");
insert into tenant(id,firstName,flatId,lastName,contactNo,gender,email) values (2,"Kartik",4,"Irpate","2222211111","Male","kartikirpate@gmail.com");


insert into maintenance(id,month,flatId,flatMaintenance,waterBill,parkingBill, others  ) values (1,"May",3,123.12,134.41,334.4,10.1);
insert into maintenance(id,month,flatId,flatMaintenance,waterBill,parkingBill,others  ) values (2,"August",2,123.12,134.43,334.4,10.1);


insert into workstaff ( id,name,password,monthSalary,address,role,contactNo,email) values (1,"Suraj Katakwar","$2a$12$wMofg9cscHN1Z9t13VfCSuGcpIOqQ3Trlf1phOzgobIk3Efkvzoky",20.00,"Bhandara","Security","3333333333","surajkatakwar@gmail.com");
insert into workstaff ( id,name,password,monthSalary,address,role,contactNo,email) values (2,"Om Hatwar","$2a$12$c72HgJclPNM3QquCYaU6s.GewHC66nOGfv4PMAzsQnjo8R6szQRdK",30.15,"Bhandara","plumber","9445599999","omhatwar@gmail.com");
insert into workstaff ( id,name,monthSalary,address,role,contactNo,email) values (3,"Bdal",301.15,"Bhandara","Cleaner","9335599999","badal@gmail.com");


insert into noticeboard (id,noticeDate,noticemsg,ownerId ) values (1,"2022-03-31","abcd",1);
insert into noticeboard (id,noticeDate,noticemsg,ownerId ) values (2,"2021-01-31","xyzz",1);

